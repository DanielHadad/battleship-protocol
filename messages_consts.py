from enum import Enum


OPTION_FORMAT = "b"
ATTACK_FORMAT = "bb"
STATUS_FORMAT = "b"


class MyEnum(Enum):
    @classmethod
    def has_value(cls, value):
        return value in cls._value2member_map_


class MessageType(MyEnum):
    CONNECTION = 1
    PREPARATION = 2
    ATTACK = 3
    ATTACK_RESPONSE = 4


class ConnectionOption(MyEnum):
    START_GAME_OFFER = 1
    START_GAME_APPROVAL = 2
    STOP_GAME = 3


class PrepareOption(MyEnum):
    READY = 1
    LISTENER_FIRST = 2
    OFFEROR_FIRST = 3


class AttackResponseStatus(MyEnum):
    MISS = 0
    HIT = 1
    DESTROY = 2
    DEFEAT = 3
