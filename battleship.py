from game import Game
from game_manager import GameManager
from battleship_communicator import BattleshipCommunicator
from connection import Connection
import socket
from exceptions import StopGameException, StopGameWasSent
from p2p_modes import OfferorMode, ListenerMode


VERSION = 1
PORT = 15000
IP = "127.0.0.1"


class BattleShip():
    """
    This class runs the battleship game again and again
    :Player _player: player who plays the game
    """

    def __init__(self, player):
        self._player = player

    def _create_offeror_communicator(self):
        """
        Creates communicator for single game in offeror mode
        :return: communicator
        :rtype: BattleshipCommunicator
        """

        # connect to other player
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ip = self._player.choose_other_player_ip()
        sock.connect((ip, PORT))

        return BattleshipCommunicator(Connection(VERSION, sock))

    def _create_listener_communicator(self):
        """
        Creates communicator for single game in listener mode
        :return: communicator
        :rtype: BattleshipCommunicator
        """

        # listen and wait for connection
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((IP, PORT))
            s.listen()

            while True:
                conn, addr = s.accept()
                communicator = BattleshipCommunicator(Connection(VERSION, conn))

                if self._player.want_to_play_with(addr):
                    return communicator
                else:
                    communicator.send_stop_game_msg()

    def _create_communicator(self):
        """
        Creates communicator for single game according to player
        :return: communicator
        :rtype: BattleshipCommunicator
        """

        if self._player.want_to_offer():
            communicator = self._create_offeror_communicator()
            mode = OfferorMode(communicator, self._player)
        else:
            communicator = self._create_listener_communicator()
            mode = ListenerMode(communicator, self._player)

        mode.connect()
        mode.prepare()

        return communicator

    def run(self):
        """
        Runs battleship game again and again
        """

        while self._player.want_to_play():
            try:
                communicator = self._create_communicator()

            except StopGameException as e:
                print(e)
                self._player.end_game()
            except StopGameWasSent as e:
                print(e)
                self._player.end_game()
            except Exception as e:
                print(e)

            else:
                game = Game(GameManager(communicator, self._player))
                game.play()
