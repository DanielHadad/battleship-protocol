from struct import pack
from messages_consts import *


class Message:
    def __init__(self, type):
        self._type = type

    def pack_data(self):
        return b''

    def get_type(self):
        return self._type


class ConnectionMessage(Message):
    def __init__(self, option):
        self._type = MessageType.CONNECTION
        self._option = option

    def pack_data(self):
        return pack(OPTION_FORMAT, self._option.value)


class PrepareMessage(Message):
    def __init__(self, option):
        self._type = MessageType.PREPARATION
        self._option = option

    def pack_data(self):
        return pack(OPTION_FORMAT, self._option.value)


class AttackMessage(Message):
    def __init__(self, i, j):
        self._type = MessageType.ATTACK
        self._i = i
        self._j = j

    def pack_data(self):
        return pack(ATTACK_FORMAT, self._i, self._j)


class AttackResponseMessage(Message):
    def __init__(self, status):
        self._type = MessageType.ATTACK_RESPONSE
        self._status = status

    def pack_data(self):
        return pack(STATUS_FORMAT, self._status.value)
