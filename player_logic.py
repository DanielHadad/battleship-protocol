from board import Board, MIN_COORDINATE, MAX_COORDINATE


class PlayerLogic:
    def get_attack_coordinates(self):
        """
        Returns player valid coordinates for an attack
        :return: valid attack
        :rtype: tuple
        """

        print("\n>> Attack Player >>")

        i = input("Enter i coordinate: ")
        j = input("Enter j coordinate: ")

        # get coordinates again if not valid
        while not (i.isdigit() and j.isdigit() and Board.are_coordinates_valid(int(i), int(j))):
            print(f"Invalid coordinates ({MIN_COORDINATE}-{MAX_COORDINATE} are valid)")
            i = input("Enter i coordinate: ")
            j = input("Enter j coordinate: ")

        return int(i), int(j)

    def handle_attack_response(self, response):
        print("Player Response: ", response.name)

    def end_game(self, won=None):
        if won is not None:
            print("\n>> You won! >>" if won else "\n>> You lost! >>")
