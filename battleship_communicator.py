from struct import unpack
from messages import *
from exceptions import StopGameException


class BattleshipCommunicator:
    """
    This class uses battleship protocol for the communication
    :Connection _connection: socket connection to send and recv messages
    """

    def __init__(self, connection):
        self._connection = connection

    def _get_msg(self, needed_type):
        """
        Return received message data
        :param MessageType needed_type: received msg type should be
        :return: msg data
        :rtype: bytes
        """

        msg_type, data = self._connection.recv_msg()

        if msg_type != needed_type:
            raise StopGameException(f"Message type should be '{needed_type}', but got {msg_type}")

        return data

    def send_start_game_offer(self):
        """
        Sends start game offer msg
        """

        self._connection.send_msg(ConnectionMessage(ConnectionOption.START_GAME_OFFER))

    def send_start_game_approval(self):
        """
        Sends start game approval msg
        """

        self._connection.send_msg(ConnectionMessage(ConnectionOption.START_GAME_APPROVAL))

    def send_stop_game_msg(self):
        """
        Sends stop game msg
        """

        self._connection.send_msg(ConnectionMessage(ConnectionOption.STOP_GAME))

    def get_connection_option(self):
        """
        Gets connection option from other player
        :return: connection option that was sent
        :rtype: ConnectionOption
        """

        option, = unpack(OPTION_FORMAT, self._get_msg(MessageType.CONNECTION))
        if not ConnectionOption.has_value(option):
            raise StopGameException(f"Invalid connection option - {option}")

        return ConnectionOption(option)

    def send_ready_request(self):
        """
        Sends ready request msg
        """

        self._connection.send_msg(PrepareMessage(PrepareOption.READY))

    def send_ready_response(self, first_player):
        """
        Sends ready response msg
        :param PrepareOption first_player: first player
        """

        self._connection.send_msg(PrepareMessage(first_player))

    def get_prepare_option(self):
        """
        Gets prepare option from other player
        :return: prepare option that was sent
        :rtype: PrepareOption
        """

        option, = unpack(OPTION_FORMAT, self._get_msg(MessageType.PREPARATION))
        if not PrepareOption.has_value(option):
            raise StopGameException(f"Invalid prepare option - {option}")

        return PrepareOption(option)

    def send_attack_request(self, i, j):
        """
        Sends attack msg
        :param int i: i coordinate
        :param int j: j coordinate
        """

        self._connection.send_msg(AttackMessage(i, j))

    def get_attack(self):
        """
        Gets attack coordinates
        :return: attack coordinates
        :rtype: tuple
        """

        return unpack(ATTACK_FORMAT, self._get_msg(MessageType.ATTACK))

    def send_attack_response(self, status):
        """
        Sends attack response msg
        :param AttackResponseStatus status: response status
        """

        self._connection.send_msg(AttackResponseMessage(status))

    def get_attack_response(self):
        """
        Gets attack response status from other player
        :return: attack response status
        :rtype: AttackResponseStatus
        """

        response, = unpack(STATUS_FORMAT, self._get_msg(MessageType.ATTACK_RESPONSE))
        if not AttackResponseStatus.has_value(response):
            raise StopGameException(f"Invalid attack response status - {response}")

        return AttackResponseStatus(response)
