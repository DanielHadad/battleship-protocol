from exceptions import StopGameException, StopGameWasSent


DO_NOT_SEND_STOP_MSG = False


class Game:
    """
    This class represents a single game
    :_manager: game manager
    """

    def __init__(self, manager):
        self._manager = manager

    def play(self):
        """
        Plays Battleship game
        """

        try:
            while not self._manager.has_ended():
                if self._manager.is_current_player():
                    self._manager.attack()
                else:
                    self._manager.response_attack()

        except StopGameException as e:
            print(e)
            self._manager.stop_game()
        except StopGameWasSent as e:
            print(e)
            self._manager.stop_game(DO_NOT_SEND_STOP_MSG)
        except Exception as e:
            print(e)
