from messages_consts import AttackResponseStatus


class GameManager:
    """
    This class manages the game
    :BattleshipCommunicator _communicator: communicator with other player
    :Player _player: player
    :bool _ended: weather game ended
    """

    def __init__(self, communicator, player):
        self._communicator = communicator
        self._player = player
        self._ended = False

    def attack(self):
        """
        Attacks enemy
        """

        # send attack
        i, j = self._player.get_attack_coordinates()
        self._communicator.send_attack_request(i, j)

        # get attack response
        response = self._communicator.get_attack_response()
        self._player.handle_attack_response(response)

        if response == AttackResponseStatus.DEFEAT:
            self._ended = True

    def response_attack(self):
        """
        Responses attack
        """

        # get attack
        i, j = self._communicator.get_attack()
        response = self._player.handle_attack(i, j)

        # send response
        self._communicator.send_attack_response(response)

        if response == AttackResponseStatus.DEFEAT:
            self._ended = True

    def is_current_player(self):
        """
        Returns weather it is player turn now
        :return: is current player
        :rtype: bool
        """

        return self._player.is_current_player()

    def has_ended(self):
        """
        Returns weather game has ended
        :return: _ended
        :rtype: bool
        """

        return self._ended

    def stop_game(self, send_stop_msg=True):
        """
        Stops the game
        """

        self._player.end_game()
        if send_stop_msg:
            self._communicator.send_stop_game_msg()
