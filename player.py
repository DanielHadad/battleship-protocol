from messages import AttackResponseStatus
from board import Board


WON = True
LOST = False


class Player:
    """
    This class represents the player in the game
    :_configuration: player configuration
    :_logic: player logic
    :_board: player board
    :_is_current_player: is current player
    """

    def __init__(self, configuration, logic):
        self._configuration = configuration
        self._logic = logic
        self._board = Board()
        self._is_current_player = False

    def set_is_current_player(self, is_current_player):
        """
        Sets _is_current_player
        """

        self._is_current_player = is_current_player
        if is_current_player:
            print("\n>> It's your turn now >>")
        else:
            print("\n>> It's the other player turn now >>")

    def is_current_player(self):
        """
        Returns weather it is player turn now
        :return: is current player
        :rtype: bool
        """

        return self._is_current_player

    def want_to_play(self):
        return self._configuration.want_to_play()

    def want_to_offer(self):
        return self._configuration.want_to_offer()

    def want_to_play_with(self, ip):
        return self._configuration.want_to_play_with(ip)

    def choose_other_player_ip(self):
        return self._configuration.choose_other_player_ip()

    def choose_first_player(self):
        return self._configuration.choose_first_player()

    def prepare(self):
        self._board.prepare()

    def get_attack_coordinates(self):
        return self._logic.get_attack_coordinates()

    def handle_attack(self, i, j):
        """
        Handles attack on player
        :return: attack response
        :rtype: AttackResponseStatus
        """

        response = self._board.attack(i, j)

        if response == AttackResponseStatus.MISS:
            self.set_is_current_player(True)
        elif response == AttackResponseStatus.DEFEAT:
            self.end_game(LOST)

        return response

    def handle_attack_response(self, response):
        """
        Handles attack response
        """

        self._logic.handle_attack_response(response)

        if response == AttackResponseStatus.MISS:
            self.set_is_current_player(False)
        elif response == AttackResponseStatus.DEFEAT:
            self.end_game(WON)

    def end_game(self, won=None):
        """
        Ends player game
        """

        self._logic.end_game(won)
        self._board = Board()
        self._is_current_player = False
