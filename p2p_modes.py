from messages import ConnectionOption, PrepareOption
from exceptions import StopGameException


class OfferorMode:
    """
    This class is used for offeror mode in p2p
    :_communicator: communicator to other player
    :_player: player
    """

    def __init__(self, communicator, player):
        self._communicator = communicator
        self._player = player

    def connect(self):
        """
        Connects to other player according to player
        """

        # send offer
        self._communicator.send_start_game_offer()

        # get approval
        connection_option = self._communicator.get_connection_option()
        if connection_option != ConnectionOption.START_GAME_APPROVAL:
            raise StopGameException(f"Connection option should be '{ConnectionOption.START_GAME_APPROVAL}', but got {connection_option}")

    def prepare(self):
        """
        Prepares player and updates other player
        """

        self._player.prepare()

        # send ready request
        self._communicator.send_ready_request()

        # get ready response
        first_player = self._communicator.get_prepare_option()
        if first_player not in (PrepareOption.LISTENER_FIRST, PrepareOption.OFFEROR_FIRST):
            raise StopGameException(f"First player '{first_player}' doesn't exist")

        self._player.set_is_current_player(first_player == PrepareOption.OFFEROR_FIRST)


class ListenerMode:
    """
    This class is used for listener mode in p2p
    :_communicator: communicator to other player
    :_player: player
    """
    
    def __init__(self, communicator, player):
        self._communicator = communicator
        self._player = player

    def connect(self):
        """
        Connects to other player according to player
        """

        # get offer
        connection_option = self._communicator.get_connection_option()
        if connection_option != ConnectionOption.START_GAME_OFFER:
            raise StopGameException(f"Connection option should be '{ConnectionOption.START_GAME_OFFER}', but got {connection_option}")

        # send approval
        self._communicator.send_start_game_approval()

    def prepare(self):
        """
        Prepares player and updates other player
        """

        self._player.prepare()

        # get ready request
        prepare_option = self._communicator.get_prepare_option()
        if prepare_option != PrepareOption.READY:
            raise StopGameException(f"Prepare option should be '{PrepareOption.READY}', but got {prepare_option}")

        # send ready response
        first_player = self._player.choose_first_player()
        self._communicator.send_ready_response(first_player)

        self._player.set_is_current_player(first_player == PrepareOption.LISTENER_FIRST)
