from messages_consts import AttackResponseStatus
from exceptions import StopGameException

MIN_COORDINATE = 1
MAX_COORDINATE = 10


class Board:
    """
    This class represents game board
    """

    def prepare(self):
        """
        Prepares the board to start the game
        """

        input("Prepare your board. Enter any key when you are ready ")

    def attack(self, i, j):
        """
        Attacks board at (i, j)
        :param int i: i coordinate
        :param int j: j coordinate
        :return: attack response
        :rtype: AttackResponseStatus
        """

        if not self.are_coordinates_valid(i, j):
            raise StopGameException(f"Invalid coordinates ({i}, {j})")

        print(f"\n>> You were attacked at ({i}, {j})! >>")

        # get response
        print("Response status options:")
        [print(f"{status.value}. {status.name}") for status in AttackResponseStatus]
        response = input("Enter your response to the attack: ")
        while not (response.isdigit() and AttackResponseStatus.has_value(int(response))):
            response = input("Response doesn't exist, enter valid response: ")

        return AttackResponseStatus(int(response))

    @staticmethod
    def are_coordinates_valid(i, j):
        """
        Returns weather coordinates are valid
        :rtype: bool
        """

        return Board._is_coordinate_valid(i) and Board._is_coordinate_valid(j)

    @staticmethod
    def _is_coordinate_valid(coordinate):
        return MIN_COORDINATE <= coordinate <= MAX_COORDINATE