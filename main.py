from player_configuration import PlayerConfiguration
from player_logic import PlayerLogic
from player import Player
from battleship import BattleShip


def main():
    player = Player(PlayerConfiguration(), PlayerLogic())
    battleship_game = BattleShip(player)

    try:
        battleship_game.run()
    except Exception as e:
        print(e)


if __name__ == '__main__':
    main()
