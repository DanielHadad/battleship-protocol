from struct import unpack, calcsize
from messages import *
from exceptions import StopGameException, StopGameWasSent


LEN_FORMAT = "b"
HEADER_FORMAT = "bb"
STOP_MSG_DATA = pack("b", ConnectionOption.STOP_GAME.value)


class Connection:
    """
    Simple socket connection
    :int _ver: protocol version
    :socket.socket _sock: socket for connection
    """

    def __init__(self, ver, sock):
        self._ver = ver
        self._sock = sock

    def __del__(self):
        self._sock.close()

    def _is_valid_version(self, ver):
        """
        Returns weather ver is valid version to handle
        :param int ver: version to check
        :return: weather ver is valid
        :rtype: bool
        """

        return ver == self._ver

    def _create_packed_msg(self, msg : Message):
        """
        Returns packed msg
        :param Message msg: msg to pack
        :return: packed msg
        :rtype: bytes
        """

        packed_msg = pack(HEADER_FORMAT, self._ver, msg.get_type().value) + msg.pack_data()
        msg_len = pack(LEN_FORMAT, len(packed_msg))

        return msg_len + packed_msg

    def send_msg(self, msg: Message):
        """
        Send msg
        :param Message msg: msg to send
        """

        packed_msg = self._create_packed_msg(msg)
        self._sock.sendall(packed_msg)

    def _recv_data(self, size):
        return self._sock.recv(size)

    def _recv_msg_len(self):
        """
        Receives msg len
        :return: msg len
        :rtype: int
        """

        msg_len = self._recv_data(calcsize(LEN_FORMAT))
        msg_len, = unpack(LEN_FORMAT, msg_len)

        return msg_len

    def _recv_msg_header(self):
        """
        Receives msg header
        :return: version, msg type
        :rtype: tuple
        """

        msg_header = self._recv_data(calcsize(HEADER_FORMAT))
        ver, msg_type = unpack(HEADER_FORMAT, msg_header)

        # check version
        if not self._is_valid_version(ver):
            raise StopGameException(f"Invalid version- {ver}")

        # check msg_type
        if not MessageType.has_value(msg_type):
            raise StopGameException(f"Invalid msg type - {msg_type}")

        return ver, MessageType(msg_type)

    def _is_stop_msg(self, msg_type, msg_data):
        """
        Returns weather msg is stop msg
        :param MessageType msg_type: msg type
        :param bytes msg_data: msg data
        :return: weather msg is stop msg
        :rtype: bool
        """

        return msg_type == MessageType.CONNECTION and msg_data == STOP_MSG_DATA

    def recv_msg(self):
        """
        Receives msg
        :return: msg type, msg data
        :rtype: tuple
        """

        msg_len = self._recv_msg_len()
        ver, msg_type = self._recv_msg_header()

        # recv msg data
        msg_data = self._recv_data(msg_len - calcsize(HEADER_FORMAT))

        if self._is_stop_msg(msg_type, msg_data):
            raise StopGameWasSent()

        return msg_type, msg_data
