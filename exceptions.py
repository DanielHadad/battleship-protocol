class MyException(Exception):
    pass


class StopGameWasSent(MyException):
    def __str__(self):
        return "\n** Game was stopped by other player: " + super().__str__()


class StopGameException(MyException):
    def __str__(self):
        return "\n** Game was stopped because of: " + super().__str__()
