from random import choice as random_choice
from messages import PrepareOption


class PlayerConfiguration:
    """
    This class is used for player configuration (player option, e.g. offer/ wait fo an offer)
    """

    def _want_to(self, msg):
        return 'y' == input(msg).lower()

    def want_to_play(self):
        return self._want_to("\nDo you want to play Battleship? (Enter 'y' to play) ")

    def want_to_offer(self):
        return self._want_to("Enter 'y' to offer a game to other user, other key to wait for an offer ")

    def want_to_play_with(self, ip):
        print("You've got a game offer from", ip)
        return self._want_to("Do you want to play with him? (Enter 'y' to play) ")

    def choose_other_player_ip(self):
        return input("Enter other player ip: ")

    def choose_first_player(self):
        return random_choice([PrepareOption.OFFEROR_FIRST, PrepareOption.LISTENER_FIRST])
